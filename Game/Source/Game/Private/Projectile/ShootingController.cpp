// Copyright Elemental Studios 2018 ©

#include "ShootingController.h"
#include "ElementProjectile.h"
#include "Engine.h"


// Sets default values for this component's properties
UShootingController::UShootingController()
{
}

void UShootingController::InitialiseController(AActor* actor)
{
	IsFired = false;

	GameActor = actor;
	MainActorController = GetWorld()->GetFirstPlayerController();

	//Get the screen size to limit the mouse and controller inputs
	ScreenSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());

	//Location we are shooting from
	ShootingLocation = GameActor->GetActorLocation();
	ShootingLocation.Z += 100;

	//Where the character is
	FVector2D characterPosOnScreen;
	MainActorController->ProjectWorldLocationToScreen(GameActor->GetActorLocation(), characterPosOnScreen, false);

	//Set the mouse location on top of the player
	//MainActorController->SetMouseLocation(characterPosOnScreen.X, characterPosOnScreen.Y);
}

void UShootingController::XAxisMovement(float x)
{
	if (x == 0) {
		return;
	}

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, "UpdateX");

	float cursorX;
	float cursorY;
	MainActorController->GetMousePosition(cursorX, cursorY);

	cursorX += x;

	if (cursorX <= 5 && x < 0) {
		return;
	}

	if (cursorX >= ScreenSize.X - 25 && x > 0) {
		return;
	}

	//MainActorController->SetMouseLocation(cursorX, cursorY);
}

void UShootingController::YAxisMovement(float y)
{
	if (y == 0) {
		return;
	}

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, "UpdateY");

	float cursorX;
	float cursorY;
	MainActorController->GetMousePosition(cursorX, cursorY);

	cursorY += y;

	if (cursorY <= 10 && y < 0) {
		return;
	}

	if (cursorY >= ScreenSize.Y - 25 && y > 0) {
		return;
	}

	//MainActorController->SetMouseLocation(cursorX, cursorY);
}

FVector2D UShootingController::GetCrosshairLocation()
{
	//Check if last input was controller or mouse
	ShootingLocation = GameActor->GetActorLocation();
	ShootingLocation.Z += 100;

	FVector2D mouseLocation;
	MainActorController->GetMousePosition(mouseLocation.X, mouseLocation.Y);
	return mouseLocation;
}

float UShootingController::GetFiringAngle()
{
	//Calculate the angle of firing
	FVector2D projectileSpawnPos;
	FVector2D cursorPos = GetCrosshairLocation();

	MainActorController->ProjectWorldLocationToScreen(ShootingLocation, projectileSpawnPos, false);

	FVector2D pointOnAxis;
	pointOnAxis = cursorPos - projectileSpawnPos;

	float angle = ((atan2(pointOnAxis.Y, pointOnAxis.X) * 180 / PI)) * -1;

	return angle;
}

void UShootingController::FireElementProjectile(ElementType type, float speed)
{
	if (IceProjectile != NULL && FireProjectile != NULL) {
		UWorld* const world = GetWorld();

		if (world != NULL) {
			FRotator spawnRotation;

			spawnRotation = GameActor->GetActorRotation(); //spawn the direction actor is facing
			ShootingLocation = GameActor->GetActorLocation() + (GameActor->GetActorForwardVector() * 50); //spawn in front of player

			FActorSpawnParameters spawnParams;
			spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	
			if (type == ElementType::FireClass) {
				AElementProjectile* actor = world->SpawnActor<AElementProjectile>(FireProjectile, ShootingLocation, spawnRotation, spawnParams);

				if (actor) {
					actor->TypeOfProjectile = ElementType::FireClass;
					actor->SetVel(speed);
				}
			}
			else {
				AElementProjectile* actor = world->SpawnActor<AElementProjectile>(IceProjectile, ShootingLocation, spawnRotation, spawnParams);

				if (actor) {
					actor->TypeOfProjectile = ElementType::IceClass;
					actor->SetVel(speed);
				}
			}
		}
	}
}

