// Fill out your copyright notice in the Description page of Project Settings.

#include "ElementProjectile.h"
#include "Components/SphereComponent.h"
#include "InteractsWithElements.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine.h"


// Sets default values
AElementProjectile::AElementProjectile()
{
	TimesBounced = 0;

	//Setup the blueprint sphere
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("ElementProjectile"); //debug purposes
	CollisionComp->OnComponentHit.AddDynamic(this, &AElementProjectile::OnHit);
	
	//Set the main root component for the blueprint
	RootComponent = CollisionComp;

	//Set the projectile to fire forwards!
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ElementProjectileMovement"));
	ProjectileMovement->UpdatedComponent = CollisionComp; //set the component we are updating with the movement
	ProjectileMovement->InitialSpeed = 700.f;
	ProjectileMovement->MaxSpeed = 2000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;

	//Lifespan of the object in seconds
	InitialLifeSpan = 30.0f;
}

void AElementProjectile::OnHit(UPrimitiveComponent* hitComponent, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit)
{
	if (otherActor->GetClass()->ImplementsInterface(UInteractsWithElements::StaticClass())) {
		SwapVisualEffect();

		switch (TypeOfProjectile) {
		case ElementType::FireClass:
			IInteractsWithElements::Execute_HitByFireElement(otherActor);
			break;
		case ElementType::IceClass:
			IInteractsWithElements::Execute_HitByIceElement(otherActor);
			break;
		}
		return;
	}

	TimesBounced++;

	if (TimesBounced < 5) {
		FActorSpawnParameters spawnParams;
		spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		if (TypeOfProjectile == ElementType::FireClass) {
			GetWorld()->SpawnActor<AActor>(FireBounce, this->GetActorLocation(), this->GetActorRotation(), spawnParams);
		}
		else {
			GetWorld()->SpawnActor<AActor>(IceBounce, this->GetActorLocation(), this->GetActorRotation(), spawnParams);
		}
	}

	if (TimesBounced > 4) {
		SwapVisualEffect();
	}
}

void AElementProjectile::SetVel(float spd)
{
	FVector velo = ProjectileMovement->Velocity;
	velo.Normalize();
	velo = velo * spd;
	ProjectileMovement->Velocity = velo;
}
