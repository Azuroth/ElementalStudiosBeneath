// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyShootingController.h"
#include "ElementProjectile.h"
#include "GameFramework/Pawn.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Engine/World.h"


// Sets default values for this component's properties
UEnemyShootingController::UEnemyShootingController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}

void UEnemyShootingController::FireElementProjectile(FVector targetLocation)
{
	FVector shootingLocation = GetOwner()->GetActorLocation();
	FVector fireDirection = targetLocation - shootingLocation;
	fireDirection.Normalize();

	UWorld* const world = GetWorld();
	FRotator spawnRotation = fireDirection.Rotation();

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	

	if (damageType == ElementType::FireClass) {
		AElementProjectile* actor = world->SpawnActor<AElementProjectile>(ElementProjectile, shootingLocation, spawnRotation, spawnParams);
	}
	else {
		AElementProjectile* actor = world->SpawnActor<AElementProjectile>(ElementProjectile, shootingLocation, spawnRotation, spawnParams);
	}
}




