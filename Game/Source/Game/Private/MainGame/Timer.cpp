// Copyright Elemental Studios 2018 ©

#include "Timer.h"

Timer::Timer()
{
	Paused = true;
}

Timer::~Timer()
{
}

//Turn milliseconds to seconds
int Timer::GetSeconds()
{
	return (int)(CurrentMilliseconds / 1000) % 60;
}

//Turn milliseconds to minutes
int Timer::GetMinutes()
{
	return (int)(CurrentMilliseconds / (1000 * 60)) % 60;
}

void Timer::TimerTick(float deltaTime)
{
	if (!Paused) {
		CurrentMilliseconds += deltaTime;
	}
}

void Timer::ResetTimer()
{
	CurrentMilliseconds = 0;
}

void Timer::SetTimerState(bool paused)
{
	Paused = paused;
}
