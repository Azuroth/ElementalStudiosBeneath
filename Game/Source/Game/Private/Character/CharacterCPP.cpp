// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterCPP.h"
#include "ShootingController.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Engine.h"


// Sets default values
ACharacterCPP::ACharacterCPP()
{
	//Create the shooting controller and attach to the character
	ShootingControllerComponent = CreateDefaultSubobject<UShootingController>(TEXT("ShootingController"));
	AddOwnedComponent(ShootingControllerComponent);

	ManaLevel = 1; //Start the mana at 1
	CurrentHealth = 4; //Start the game with 4 hearts

	iceCharging = false;
	fireCharging = false;

	iceCharge = 700.f;
	fireCharge = 700.f;
}

// Called when the game starts or when spawned
void ACharacterCPP::BeginPlay()
{
	Super::BeginPlay();

	PrimaryActorTick.bCanEverTick = true;

	ShootingControllerComponent->InitialiseController(this);
}

// Called every frame
void ACharacterCPP::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	if (!ShieldUp)
	{
		if (CanRegenMana) {
			if (ManaLevel < 1) {
				ManaLevel += 0.2 * deltaTime; //Increase bar smoothly

				if (ManaLevel > 1) {
					ManaLevel = 1; //max of 1
				}
			}
		}
		else {
			TimeKeeper += deltaTime;

			if (TimeKeeper > 1) { //2 seconds gap before we can regen mana
				TimeKeeper = 0;
				CanRegenMana = true;
			}
		}

		if (iceCharging && iceCharge < 2000) {
			iceCharge += 500 * deltaTime;
		}
		if (fireCharging && fireCharge < 2000) {
			fireCharge += 500 * deltaTime;
		}
	}
	else
	{
		iceCharging = false;
		fireCharging = false;
	}
}

// Called to bind functionality to input
void ACharacterCPP::SetupPlayerInputComponent(UInputComponent* playerInputComponent)
{
	Super::SetupPlayerInputComponent(playerInputComponent);

	//Bind the method to the keypress event in action bindings
	playerInputComponent->BindAction("FireIce", IE_Pressed, this, &ACharacterCPP::ChargeIceProjectile);
	playerInputComponent->BindAction("FireFire", IE_Pressed, this, &ACharacterCPP::ChargeFireProjectile);

	playerInputComponent->BindAction("FireIce", IE_Released, this, &ACharacterCPP::FireIceElement);
	playerInputComponent->BindAction("FireFire", IE_Released, this, &ACharacterCPP::FireFireElement);
}

//Main Methods
void ACharacterCPP::FireIceElement()
{
	if (ManaLevel >= 0.1f && CanShoot && iceCharging) {
		iceCharging = false;
		ShootingControllerComponent->IsFired = true;
		CanRegenMana = false;
		TimeKeeper = 0;

		ManaLevel = ManaLevel - .1f;
		ShootingControllerComponent->FireElementProjectile(ElementType::IceClass, iceCharge);
		fireCharge = 700.f;
		iceCharge = 700.f;
	}
	if (ManaLevel < 0.1f)
	{
		DisplayText = "Not enough mana to fire projectile!";

		FTimerHandle UnusedHandle;
		GetWorldTimerManager().SetTimer(
			UnusedHandle, this, &ACharacterCPP::RemoveText, 2, false);
	}
	else {
		fireCharging = false;
		iceCharging = false;
		fireCharge = 700.f;
		iceCharge = 700.f;
	}
}

void ACharacterCPP::RemoveText()
{
	DisplayText = "";
}

void ACharacterCPP::FireFireElement()
{
	if (ManaLevel >= 0.1f && CanShoot && fireCharging) {
		fireCharging = false;
		ShootingControllerComponent->IsFired = true;
		CanRegenMana = false;
		TimeKeeper = 0;

		ManaLevel = ManaLevel - .1f;
		ShootingControllerComponent->FireElementProjectile(ElementType::FireClass, fireCharge);
		fireCharge = 700.f;
		iceCharge = 700.f;
	}
	if (ManaLevel < 0.1f)
	{
		DisplayText = "Not enough mana to fire projectile!";

		FTimerHandle UnusedHandle;
		GetWorldTimerManager().SetTimer(
			UnusedHandle, this, &ACharacterCPP::RemoveText, 2, false);
	}
	else 
	{
		fireCharging = false;
		iceCharging = false;
		fireCharge = 700.f;
		iceCharge = 700.f;
	}
}

void ACharacterCPP::ChargeFireProjectile()
{
	if (!iceCharging && !ShieldUp) {
		fireCharging = true;
	}
}

void ACharacterCPP::ChargeIceProjectile()
{
	if (!fireCharging && !ShieldUp) {
		iceCharging = true;
	}
}
