// Fill out your copyright notice in the Description page of Project Settings.

#include "BeneathGameInstance.h"

#define ROOM_FOLDER_PATH "/Game/Blueprints/Rooms"
#define ENEMY_FOLDER_PATH "/Game/Blueprints/AI"

UBeneathGameInstance::UBeneathGameInstance(const FObjectInitializer& objectInitialiser)
{
	isRoomDataLoaded = false;
	isEnemyDataLoaded = false;
}

void UBeneathGameInstance::LoadData()
{
	LoadRooms();
	isRoomDataLoaded = true;

	LoadEnemies();
	isEnemyDataLoaded = true;

	// LoadRoomsAsync();
	// LoadEnemiesAsync();
}

bool UBeneathGameInstance::IsDataLoaded()
{
	return isRoomDataLoaded && isEnemyDataLoaded;
}

void UBeneathGameInstance::LoadRoomsAsync()
{
	roomBlueprintLibrary = UObjectLibrary::CreateLibrary(UObject::StaticClass(), true, true);
	roomBlueprintLibrary->AddToRoot();
	roomBlueprintLibrary->LoadBlueprintAssetDataFromPath(TEXT(ROOM_FOLDER_PATH));

	roomBlueprintLibrary->GetAssetDataList(roomAssets);

	TArray<FSoftObjectPath> itemsToStream;
	for (int i = 0; i < roomAssets.Num(); ++i) {
		itemsToStream.AddUnique(roomAssets[i].ToStringReference());
	}

	UE_LOG(LogTemp, Warning, TEXT("Began loading rooms."));
	streamableManager.RequestAsyncLoad(itemsToStream, FStreamableDelegate::CreateUObject(this, &UBeneathGameInstance::LoadRoomsCallback));
}

// Loads all rooms in the room blueprints folder into the array.
// See: https://answers.unrealengine.com/questions/89482/dynamic-blueprints-loading.html#answer-309155
void UBeneathGameInstance::LoadRoomsCallback()
{
	for (int i = 0; i < roomAssets.Num(); ++i) {
		FAssetData& currentData = roomAssets[i];
		UBlueprint* blueprint = Cast<UBlueprint>(currentData.GetAsset());

		if (blueprint) {
			// If we're in editor
			TSubclassOf<UObject> generatedClass = blueprint->GeneratedClass;

			if (generatedClass->IsChildOf(ARoomBlueprint::StaticClass())) {
				RoomBlueprints.Add(Cast<ARoomBlueprint>(generatedClass->GetDefaultObject()));
			}
		}
		else {
			// Funky stuff with runtime generated classes
			FString generatedClassName = currentData.AssetName.ToString() + "_C";
			UClass* runtimeClass = FindObject<UClass>(currentData.GetPackage(), *generatedClassName);

			if (runtimeClass) {
				if (runtimeClass->IsChildOf(ARoomBlueprint::StaticClass())) {
					RoomBlueprints.Add(Cast<ARoomBlueprint>(runtimeClass->GetDefaultObject()));
				}
			}
			else {
				UObjectRedirector* renamedClassDirector = FindObject<UObjectRedirector>(currentData.GetPackage(), *generatedClassName);
				if (renamedClassDirector) {
					UClass* renamedClass = CastChecked<UClass>(renamedClassDirector->DestinationObject);

					if (renamedClass->IsChildOf(ARoomBlueprint::StaticClass())) {
						RoomBlueprints.Add(Cast<ARoomBlueprint>(renamedClass->GetDefaultObject()));
					}
				}
				else {
					// Something has gone horribly wrong
					UE_LOG(LogTemp, Warning, TEXT("Unable to load room."));
				}
			}
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("Room data loaded."));
	isRoomDataLoaded = true;
}

void UBeneathGameInstance::LoadEnemiesAsync()
{
	enemyBlueprintLibrary = UObjectLibrary::CreateLibrary(UObject::StaticClass(), true, true);
	enemyBlueprintLibrary->AddToRoot();
	enemyBlueprintLibrary->LoadBlueprintAssetDataFromPath(TEXT(ENEMY_FOLDER_PATH));

	enemyBlueprintLibrary->GetAssetDataList(enemyAssets);

	TArray<FSoftObjectPath> itemsToStream;
	for (int i = 0; i < enemyAssets.Num(); ++i) {
		itemsToStream.AddUnique(enemyAssets[i].ToStringReference());
	}

	UE_LOG(LogTemp, Warning, TEXT("Began loading enemies."));
	streamableManager.RequestAsyncLoad(itemsToStream, FStreamableDelegate::CreateUObject(this, &UBeneathGameInstance::LoadEnemiesCallback));
}

// Loads all enemies in the enemy blueprints folder into the array.
// See: https://answers.unrealengine.com/questions/89482/dynamic-blueprints-loading.html#answer-309155
void UBeneathGameInstance::LoadEnemiesCallback()
{
	for (int i = 0; i < enemyAssets.Num(); ++i) {
		FAssetData& currentData = enemyAssets[i];
		UBlueprint* blueprint = Cast<UBlueprint>(currentData.GetAsset());

		if (blueprint) {
			// If we're in editor
			TSubclassOf<UObject> generatedClass = blueprint->GeneratedClass;

			if (generatedClass->IsChildOf(ACharacter_Enemy::StaticClass())) {
				EnemyBlueprints.Add(Cast<ACharacter_Enemy>(generatedClass->GetDefaultObject()));
			}
		}
		else {
			// Funky stuff with runtime generated classes
			FString generatedClassName = currentData.AssetName.ToString() + "_C";
			UClass* runtimeClass = FindObject<UClass>(currentData.GetPackage(), *generatedClassName);

			if (runtimeClass) {
				if (runtimeClass->IsChildOf(ACharacter_Enemy::StaticClass())) {
					EnemyBlueprints.Add(Cast<ACharacter_Enemy>(runtimeClass->GetDefaultObject()));
				}
			}
			else {
				UObjectRedirector* renamedClassDirector = FindObject<UObjectRedirector>(currentData.GetPackage(), *generatedClassName);
				if (renamedClassDirector) {
					UClass* renamedClass = CastChecked<UClass>(renamedClassDirector->DestinationObject);

					if (renamedClass->IsChildOf(ACharacter_Enemy::StaticClass())) {
						EnemyBlueprints.Add(Cast<ACharacter_Enemy>(renamedClass->GetDefaultObject()));
					}
				}
				else {
					// Something has gone horribly wrong
					UE_LOG(LogTemp, Warning, TEXT("Unable to load enemy."));
				}
			}
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("Enemy data loaded."));
	isEnemyDataLoaded = true;
}

// Loads all rooms in the room blueprints folder into the array.
// See: https://answers.unrealengine.com/questions/89482/dynamic-blueprints-loading.html#answer-309155
void UBeneathGameInstance::LoadRooms()
{
	UObjectLibrary* objectLibrary;

	// Initialise the object library
	objectLibrary = UObjectLibrary::CreateLibrary(UObject::StaticClass(), true, true);

	// Load the asset data from within the rooms folder into the object library
	objectLibrary->LoadBlueprintAssetDataFromPath(TEXT(ROOM_FOLDER_PATH));

	// Retrieve the asset data
	TArray<FAssetData> assetData;
	objectLibrary->GetAssetDataList(assetData);

	// Iterate through each asset and load its generated blueprint class into
	// the rooms array.
	for (int i = 0; i < assetData.Num(); ++i) {
		FAssetData& currentData = assetData[i];
		UBlueprint* blueprint = Cast<UBlueprint>(currentData.GetAsset());

		if (blueprint) {
			// If we're in editor
			TSubclassOf<UObject> generatedClass = blueprint->GeneratedClass;

			if (generatedClass->IsChildOf(ARoomBlueprint::StaticClass())) {
				RoomBlueprints.Add(Cast<ARoomBlueprint>(generatedClass->GetDefaultObject()));
			}
		}
		else {
			// Funky stuff with runtime generated classes
			FString generatedClassName = currentData.AssetName.ToString() + "_C";
			UClass* runtimeClass = FindObject<UClass>(currentData.GetPackage(), *generatedClassName);

			if (runtimeClass) {
				if (runtimeClass->IsChildOf(ARoomBlueprint::StaticClass())) {
					RoomBlueprints.Add(Cast<ARoomBlueprint>(runtimeClass->GetDefaultObject()));
				}
			}
			else {
				UObjectRedirector* renamedClassDirector = FindObject<UObjectRedirector>(currentData.GetPackage(), *generatedClassName);
				if (renamedClassDirector) {
					UClass* renamedClass = CastChecked<UClass>(renamedClassDirector->DestinationObject);

					if (renamedClass->IsChildOf(ARoomBlueprint::StaticClass())) {
						RoomBlueprints.Add(Cast<ARoomBlueprint>(renamedClass->GetDefaultObject()));
					}
				}
				else {
					// Something has gone horribly wrong
					UE_LOG(LogTemp, Warning, TEXT("Unable to load room."));
				}
			}
		}
	}
}

void UBeneathGameInstance::LoadEnemies()
{
	UObjectLibrary* objectLibrary;

	// Initialise the object library
	objectLibrary = UObjectLibrary::CreateLibrary(UObject::StaticClass(), true, true);

	// Load the asset data from within the AI folder into the object library
	objectLibrary->LoadBlueprintAssetDataFromPath(TEXT(ENEMY_FOLDER_PATH));

	// Retrieve the asset data
	TArray<FAssetData> assetData;
	objectLibrary->GetAssetDataList(assetData);

	// Iterate through each asset and load its generated blueprint class into
	// the enemies array.
	for (int i = 0; i < assetData.Num(); ++i) {
		FAssetData& currentData = assetData[i];
		UBlueprint* blueprint = Cast<UBlueprint>(currentData.GetAsset());

		if (blueprint) {
			// If we're in editor
			TSubclassOf<UObject> generatedClass = blueprint->GeneratedClass;

			if (generatedClass->IsChildOf(ACharacter_Enemy::StaticClass())) {
				EnemyBlueprints.Add(Cast<ACharacter_Enemy>(generatedClass->GetDefaultObject()));
			}
		}
		else {
			// Funky stuff with runtime generated classes
			FString generatedClassName = currentData.AssetName.ToString() + "_C";
			UClass* runtimeClass = FindObject<UClass>(currentData.GetPackage(), *generatedClassName);

			if (runtimeClass) {
				if (runtimeClass->IsChildOf(ACharacter_Enemy::StaticClass())) {
					EnemyBlueprints.Add(Cast<ACharacter_Enemy>(runtimeClass->GetDefaultObject()));
				}
			}
			else {
				UObjectRedirector* renamedClassDirector = FindObject<UObjectRedirector>(currentData.GetPackage(), *generatedClassName);
				if (renamedClassDirector) {
					UClass* renamedClass = CastChecked<UClass>(renamedClassDirector->DestinationObject);

					if (renamedClass->IsChildOf(ACharacter_Enemy::StaticClass())) {
						EnemyBlueprints.Add(Cast<ACharacter_Enemy>(renamedClass->GetDefaultObject()));
					}
				}
				else {
					// Something has gone horribly wrong
					UE_LOG(LogTemp, Warning, TEXT("Unable to load enemy."));
				}
			}
		}
	}
}
