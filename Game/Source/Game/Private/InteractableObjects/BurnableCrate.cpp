// Fill out your copyright notice in the Description page of Project Settings.

#include "BurnableCrate.h"
#include "Engine.h"


// Sets default values
ABurnableCrate::ABurnableCrate()
{
	PrimaryActorTick.bCanEverTick = true;

	OnFire = false;
	CurrentTime = 0;
	SecondsTillDeath = 2;
}

// Called when the game starts or when spawned
void ABurnableCrate::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABurnableCrate::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	//Give the box a time till it gets fully destroyed
	//Next bit is only called if the box has been hit by a fire element
	if (OnFire) {
		CurrentTime += deltaTime;

		if (CurrentTime > SecondsTillDeath) { 
			Destroy();
		}
	}
}

void ABurnableCrate::HitByIceElement_Implementation()
{
	//Stop the tick event
	OnFire = false;
}

void ABurnableCrate::HitByFireElement_Implementation()
{
	//So the tick event runs
	OnFire = true;
}

