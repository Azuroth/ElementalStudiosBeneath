// Fill out your copyright notice in the Description page of Project Settings.

#include "../../Public/UI/Minimap.h"

AMinimap::AMinimap()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryActorTick.bCanEverTick = true;
}

void AMinimap::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	// Move this to the player's position
	FVector playerPosition = playerCharacter->GetActorLocation();

	FVector newPosition = FVector(playerPosition.X, MinimapZoom, playerPosition.Z);

	this->SetActorLocation(newPosition);
}

// Called when the game starts
void AMinimap::BeginPlay()
{
	Super::BeginPlay();

	// Get a reference to the player character
	playerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
}
