// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemySpawnPoints.h"


// Sets default values for this component's properties
UEnemySpawnPoints::UEnemySpawnPoints()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UEnemySpawnPoints::BeginPlay()
{
	Super::BeginPlay();

	CreatePatrolDataList();	
}

TArray<FEnemyPatrolData> UEnemySpawnPoints::GetPatrolData()
{
	return patrolData;
}

void UEnemySpawnPoints::CreatePatrolDataList()
{
	//Get all spawn points attached to the this USceneComponent
	TArray<USceneComponent*> spawnPoints;
	GetChildrenComponents(false, spawnPoints);
	for (int i = 0; i < spawnPoints.Num(); i++)
	{
		FEnemyPatrolData nextData;
		FVector spawnP = spawnPoints[i]->GetComponentLocation();
		nextData.SpawnPoint = spawnP;
		UE_LOG(LogTemp, Warning, TEXT("New Spawn Point: %s"), *spawnP.ToString());
		//Get all patrol points attached to each spawn point
		TArray<USceneComponent*> patrolPoints = spawnPoints[i]->GetAttachChildren();
		for (int i = 0; i < patrolPoints.Num(); i++)
		{
			//Get the location of each patrol point and add to the struct list
			FVector patrolLocation = patrolPoints[i]->GetComponentLocation();
			if (patrolLocation != spawnP) {
				UE_LOG(LogTemp, Warning, TEXT("Patrol Point Added: %s"), *patrolLocation.ToString());
				nextData.PatrolPoints.Add(patrolLocation);
			}			
		}

		//Check if the enemy has patrol points
		if (nextData.PatrolPoints.Num() > 0) {
			patrolData.Add(nextData);
		}
		//Wandering enemy would not require patrol points
	}
}


