// Fill out your copyright notice in the Description page of Project Settings.

#include "Character_Enemy.h"
#include "Engine/World.h"
#include "AI/Navigation/NavigationPath.h"
#include "AI/Navigation/NavigationSystem.h"
#include "ElementProjectile.h"
#include "Classes/Components/CapsuleComponent.h"
#include "DrawDebugHelpers.h"


// Sets default values
ACharacter_Enemy::ACharacter_Enemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	EnemyShootingComponent = CreateDefaultSubobject<UEnemyShootingController>(TEXT("EnemyShootingController"));
}

// Called when the game starts or when spawned
void ACharacter_Enemy::BeginPlay()
{
	Super::BeginPlay();
	controllerAI = GetController();
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ACharacter_Enemy::BeginOverlap);

	additionToAdd = 15000 / (FireRate / 0.1f);
	scaleLevel = 1;
}

// Called every frame
void ACharacter_Enemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (rotateToPoint) {
		InterpToTargetRotation(DeltaTime);
	}

	if (shotTimer > 0) {
		shotTimer -= DeltaTime;
		if (shotTimer < 0) {
			shotTimer = 0;
		}
	}

	if (startChargingShot && shotTimer == 0) { //Charge shot
		chargeTimer += DeltaTime;
		if (chargeTimer > FireRate) { //Stop charging shot and fire if == fireRate
			chargeTimer = 0;	
			startChargingShot = false;
			chargePointLightRef->SetIntensity(0);
			FireShot();
			shotTimer = 0.5f;
		}
		else {
			ChargeShot(DeltaTime);
		}
	}

	if (startEnemySizeChange) {
		ChangeEnemySize(DeltaTime);
	}
}

void ACharacter_Enemy::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (AElementProjectile* proj = Cast<AElementProjectile>(OtherActor)) {	
		if (!proj->enemyProjectile) {
			if (proj->TypeOfProjectile == EnemyShootingComponent->damageType) {
				proj->SwapVisualEffect();
				if (scaleLevel != 1) {
					scaleLevel -= 0.4f;
					startEnemySizeChange = true;
				}
				else {
					Destroy();
				}
				
			}
			else { //Hit enemy with wrong projectile
				if (scaleLevel < 1.8f) {
					scaleLevel += 0.4f;
					startEnemySizeChange = true;					
				}
				GetController()->StopMovement();
				FRotator newRot = (playerCharacterRef->GetActorLocation() - GetActorLocation()).Rotation();
				newRot.Pitch = 0;
				SetActorRotation(newRot, ETeleportType::None);
				Fire(playerCharacterRef->GetActorLocation());
				proj->SwapVisualEffect();
			}
		}		
	}
}

// Called to bind functionality to input
void ACharacter_Enemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ACharacter_Enemy::SetTargetRotation(FVector startPos, FVector targetPos)
{
	if (startPos.X < targetPos.X) {
		
		targetRot = FRotator(0.0, 0, 0.0);
	}
	else {
		targetRot = FRotator(0.0, -180, 0.0);
	}
}

void ACharacter_Enemy::StartRotation()
{
	rotateToPoint = true;
}

void ACharacter_Enemy::Fire(FVector targetLocation)
{
	EnemyShootingComponent->FireElementProjectile(targetLocation);
}

bool ACharacter_Enemy::CanBeginPathFind()
{
	if (playerCharacterRef) { //Check if the playerCharacterRef has been correctly found
		FVector enemyPos = GetActorLocation();
		FVector playerPos = playerCharacterRef->GetActorLocation();
		if (FMath::Abs(playerPos.X - enemyPos.X) < 4000 && FMath::Abs(playerPos.Y - enemyPos.Y) < 1000) { //Only allow enemies to pathfind within a certain radius
			return true;
		}
	}
	else {
		//playerCharacterRef = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	}
	
	return false;
}

bool ACharacter_Enemy::GenerateNavMesh()
{
	if (playerCharacterRef) { //Check if the playerCharacterRef has been correctly found
		FVector enemyPos = GetActorLocation();
		FVector playerPos = playerCharacterRef->GetActorLocation();
		if (FMath::Abs(playerPos.X - enemyPos.X) < 5000 && FMath::Abs(playerPos.Y - enemyPos.Y) < 1000) { //Only allow enemies to pathfind within a certain radius
			return true;
		}
	}
	else {
		//playerCharacterRef = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	}

	return false;
}

void ACharacter_Enemy::LightPointRef(UPointLightComponent * pointRef)
{
	chargePointLightRef = pointRef;
}

void ACharacter_Enemy::ChargeShot(bool modifier)
{
	startChargingShot = modifier;
	if (!startChargingShot) {
		chargePointLightRef->SetIntensity(0);
		chargeTimer = 0;
	}	
}

void ACharacter_Enemy::InterpToTargetRotation(float deltaTime)
{
	//Check if the yaw value is within a 3 float range
	float rangeVal = GetActorRotation().Yaw;
	if (rangeVal > targetRot.Yaw - 3 && rangeVal < targetRot.Yaw + 3) {
		rotateToPoint = false;
		RotationFinished();
		return;
	}

	float currentYaw = GetActorRotation().Yaw;
	float targetYaw = targetRot.Yaw;

	//Interpolate the enemy yaw value to the target yaw value
	float newYaw = FMath::FInterpTo(currentYaw, targetYaw, deltaTime, 15.0f);
	FRotator newRotation(0.0, newYaw, 0.0);
	//controllerAI->SetControlRotation(newRotation);
    SetActorRotation(newRotation);
}

void ACharacter_Enemy::ChargeShot(float deltaTime)
{
	float intensityToAdd = additionToAdd / (0.1 / deltaTime);
	float currentIntensity = chargePointLightRef->Intensity;

	currentIntensity += intensityToAdd;
	chargePointLightRef->SetIntensity(currentIntensity);
}

void ACharacter_Enemy::ChangeEnemySize(float deltaTime)
{
	FVector enemyScale = GetActorScale();	
	float scaleInterp = FMath::FInterpTo(enemyScale.X, scaleLevel, deltaTime, 0.01f);

	if (scaleInterp >= (scaleLevel - 0.05f) || scaleInterp <= (scaleLevel + 0.05f)) {
		startEnemySizeChange = false;
		scaleInterp = scaleLevel;
	}
	UE_LOG(LogTemp, Warning, TEXT("ScaleLevel: %f  CScale: %f"), scaleLevel, scaleInterp);
	FVector newScale(scaleInterp, scaleInterp, scaleInterp);
	SetActorScale3D(newScale);	
}

void ACharacter_Enemy::PassPlayerReference(ACharacter * playerRef)
{
	playerCharacterRef = playerRef;
}

void ACharacter_Enemy::UpdatePatrolPoints(FEnemyPatrolData* patrolData)
{
	SpawnLocation = patrolData->SpawnPoint;
	patrolPoints = patrolData->PatrolPoints;
}

FVector ACharacter_Enemy::GetNextPatrolPoint()
{
	FVector nextPoint = patrolPoints[targetPatrolPoint];
	targetPatrolPoint++;
	if (targetPatrolPoint >= patrolPoints.Num()) {
		targetPatrolPoint = 0;
	}

	return nextPoint;
}




