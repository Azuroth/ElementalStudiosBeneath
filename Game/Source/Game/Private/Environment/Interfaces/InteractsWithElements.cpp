// Fill out your copyright notice in the Description page of Project Settings.

#include "InteractsWithElements.h"


// Add default functionality here for any IInteractsWithElements functions that are not pure virtual.

UInteractsWithElements::UInteractsWithElements(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{

}

void IInteractsWithElements::HitByFireElement_Implementation()
{
	//blank because there is no default behaviour
}

void IInteractsWithElements::HitByIceElement_Implementation()
{
	//blank because there is no default behaviour
}