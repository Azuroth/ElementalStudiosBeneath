// Fill out your copyright notice in the Description page of Project Settings.

#include "MapGeneratorWorker.h"

#define BLOCK_WIDTH 400
#define BLOCK_HEIGHT 400

FMapGeneratorWorker* FMapGeneratorWorker::Runnable = NULL;

FMapGeneratorWorker::FMapGeneratorWorker(
	TArray<RoomSpawnInformation*>* dataLocation,
	FVector* playerSpawnPositionLocation,
	int newNumberOfRooms,
	int newNumberOfCheckpoints,
	Biome newBiome,
	UBeneathGameInstance* beneathGameInstance,
	FLevelDelegate newLevelDelegate,
	FRandomStream* randomStream)
{
	MapData = dataLocation;
	PlayerSpawnPosition = playerSpawnPositionLocation;
	numberOfRooms = newNumberOfRooms;
	numberOfCheckpoints = newNumberOfCheckpoints;
	biome = newBiome;
	gameInstance = beneathGameInstance;
	callback = newLevelDelegate;

	this->randomStream = randomStream;

	IsFinished = false;

	Thread = FRunnableThread::Create(this, TEXT("FMapGeneratorWorker"), 0, TPri_Normal);
}

FMapGeneratorWorker::~FMapGeneratorWorker()
{
	delete Thread;
	Thread = NULL;
}

bool FMapGeneratorWorker::Init()
{
	return true;
}

uint32 FMapGeneratorWorker::Run()
{
	// Initial wait
	FPlatformProcess::Sleep(0.3);

	UE_LOG(LogTemp, Warning, TEXT("Worker starting. Rooms: %d"), numberOfRooms);
	GenerateLevel();
	callback.ExecuteIfBound();
	IsFinished = true;

	return 0;
}

FMapGeneratorWorker* FMapGeneratorWorker::InitRun(
	TArray<RoomSpawnInformation*>* dataLocation,
	FVector* playerSpawnPositionLocation,
	int newNumberOfRooms,
	int newNumberOfCheckpoints,
	Biome newBiome,
	UBeneathGameInstance* beneathGameInstance,
	FLevelDelegate newLevelDelegate,
	FRandomStream* randomStream)
{
	//Create new instance of thread if it does not exist
	//		and the platform supports multi threading!
	if (!Runnable && FPlatformProcess::SupportsMultithreading())
	{
		Runnable = new FMapGeneratorWorker(dataLocation, playerSpawnPositionLocation, newNumberOfRooms, newNumberOfCheckpoints, newBiome, beneathGameInstance, newLevelDelegate, randomStream);
	}
	return Runnable;
}

void FMapGeneratorWorker::Shutdown()
{
	if (Runnable)
	{
		Runnable->EnsureCompletion();
		delete Runnable;
		Runnable = NULL;
	}
}

bool FMapGeneratorWorker::IsThreadFinished()
{
	if (Runnable) {
		return Runnable->IsFinished;
	}
	return true;
}

void FMapGeneratorWorker::Stop()
{
}

void FMapGeneratorWorker::EnsureCompletion()
{
	Stop();
	Thread->WaitForCompletion();
}

bool FMapGeneratorWorker::CheckCollision(int x, int y, TArray<CollisionLocation>* collisionMap)
{
	return collisionMap->Contains(CollisionLocation(x, y));
}

void FMapGeneratorWorker::AddRoom(int x, int y, int roomIndex, TArray<FEntrance>* openList, TArray<CollisionLocation>* collisionMap)
{
	// Update the collision map
	for (int curX = x; curX < x + gameInstance->RoomBlueprints[roomIndex]->Width; ++curX) {
		for (int curY = y; curY < y + gameInstance->RoomBlueprints[roomIndex]->Height; ++curY) {
			collisionMap->Emplace(curX, curY);
		}
	}

	// Get the locations of the new room's entrances
	TArray<FEntrance> newEntrances;
	GetEntrancesForRoom(roomIndex, &newEntrances, true);

	// For each of the new entrances, check if they are facing any solid
	// tiles. If they aren't then add them to the open list.
	for (int currentNewEntranceIndex = 0; currentNewEntranceIndex < newEntrances.Num(); ++currentNewEntranceIndex) {
		int entranceX = x + newEntrances[currentNewEntranceIndex].X;
		int entranceY = y + newEntrances[currentNewEntranceIndex].Y;

		if (CheckCollision(entranceX, entranceY, collisionMap) == false) {
			openList->Emplace(entranceX, entranceY, newEntrances[currentNewEntranceIndex].Type);
		}
	}

	// Add the room to the array
	(*MapData).Add(new RoomSpawnInformation(x, y, roomIndex));
}

void FMapGeneratorWorker::GetEntrancesForRoom(int roomIndex, TArray<FEntrance>* newEntrances, bool getExternal)
{
	TArray<FEntrance> blueprintEntrances;

	// Only get the entrances if the room has them in the first place
	if (gameInstance->RoomBlueprints[roomIndex]->GetNumberOfEntrances() > 0) {
		// Get the entrances of the room
		blueprintEntrances = gameInstance->RoomBlueprints[roomIndex]->Entrances;

		// If the getExternal argument is true, then get the position of the
		// square that connects to the entrance. Otherwise get the position of the 
		// block in the room that has the entrance. For example, if a 1x1 room has a 
		// single left facing entrance, the internal position of the entrance will be
		// [0, 0], while the external position will be [-1, 0].
		if (getExternal) {
			// For each of the entrances, get the position of the tile that connects to
			// them and add this to the new entrances array.
			for (int i = 0; i < blueprintEntrances.Num(); ++i) {
				FVector2D* entrancePosition = blueprintEntrances[i].GetPosition();
				newEntrances->Emplace(entrancePosition->X, entrancePosition->Y, blueprintEntrances[i].Type);
			}
		}
		else {
			newEntrances->Append(blueprintEntrances);
		}
	}
}

bool FMapGeneratorWorker::CanRoomBeJoined(FEntrance entranceToJoinOn, int roomIndex, FVector2D* roomPosition, TArray<CollisionLocation>* collisionMap)
{
	// Get the locations of the entrances within the room
	TArray<FEntrance> roomEntrances;
	GetEntrancesForRoom(roomIndex, &roomEntrances);

	// Left entrances can only be joined to right entrances, tops to bottom etc.
	// Check to see which entrances need to be filtered
	EntranceType requiredEntranceType;

	switch (entranceToJoinOn.Type)
	{
	case EntranceType::Top:
		requiredEntranceType = EntranceType::Bottom;
		break;
	case EntranceType::Right:
		requiredEntranceType = EntranceType::Left;
		break;
	case EntranceType::Bottom:
		requiredEntranceType = EntranceType::Top;
		break;
	case EntranceType::Left:
		requiredEntranceType = EntranceType::Right;
		break;
	}

	for (int currentEntranceIndex = 0; currentEntranceIndex < roomEntrances.Num(); ++currentEntranceIndex) {
		bool roomCanBePlaced = true;

		// Work out where the origin of the room will be if it is placed by
		// connecting this entrance.
		int roomX = entranceToJoinOn.X - roomEntrances[currentEntranceIndex].X;
		int roomY = entranceToJoinOn.Y - roomEntrances[currentEntranceIndex].Y;

		// If this entrance is compatible
		if (roomEntrances[currentEntranceIndex].Type == requiredEntranceType) {
			// Check the collision map to see if the room can be placed at this
			// location without colliding with any existing rooms.
			for (int curX = roomX; curX < roomX + gameInstance->RoomBlueprints[roomIndex]->Width; ++curX) {
				for (int curY = roomY; curY < roomY + gameInstance->RoomBlueprints[roomIndex]->Height; ++curY) {
					if (CheckCollision(curX, curY, collisionMap)) {
						roomCanBePlaced = false;
						break;
					}
				}
				if (!roomCanBePlaced) {
					break;
				}
			}

			// If the room can be placed, update the roomPosition variable and return
			// true.
			if (roomCanBePlaced) {
				roomPosition->X = roomX;
				roomPosition->Y = roomY;
				return true;
			}
		}
	}

	// If we've reached here then the room cannot be joined at this entrance
	return false;
}

FVector* FMapGeneratorWorker::ConvertBlockPositionToWorldPosition(float x, float y)
{
	// Convert a position from block space to world space. Due to the origin being
	// treated as the top left, the y co-ordinate needs to be inverted.
	return new FVector(x * BLOCK_WIDTH, 0, -y * BLOCK_HEIGHT);
}

void FMapGeneratorWorker::GenerateLevel()
{
	TArray<FEntrance> openList;					// Maintains a list of open entrances
	TArray<CollisionLocation> collisionMap;		// Maintains a list of tiles that are filled
	TMap<int, int> placedRoomInstances;			// Stores room indices and a value indicating how many times they have been placed in the level so far
	TMap<BorderBlockType, int> borderBlocks;	// Stores a list of the indexes associated with each border block type

	int levelEntranceIndex = -1;				// Index of the level entrance room for this biome
	int levelExitIndex = -1;					// Index of the level exit room for this biome
	int levelCheckpointIndex = -1;				// Index of the level checkpoint room for this biome
	int levelTutorialIndex = -1;				// Index of the level tutorial (dungeon biome)
	int blockIndex = -1;						// Index of the 1x1 block for this biome

	bool allBorderBlocksExist = true;			// Set to true if all border block types are present for the biome

												// Find the 'block' (a 1x1 cube with no entrances), level entrance, level checkpoint and
												// level exit rooms for this biome.
	for (int i = 0; i < gameInstance->RoomBlueprints.Num(); ++i) {
		if (gameInstance->RoomBlueprints[i]->Biome == biome) {
			if (gameInstance->RoomBlueprints[i]->IsLevelEntrance) {
				levelEntranceIndex = i;
			}
			else if (gameInstance->RoomBlueprints[i]->IsLevelExit) {
				levelExitIndex = i;
			}
			else if (gameInstance->RoomBlueprints[i]->GetNumberOfEntrances() == 0 &&
				gameInstance->RoomBlueprints[i]->BorderType == BorderBlockType::None) {
				blockIndex = i;
			}
			else if (gameInstance->RoomBlueprints[i]->IsLevelCheckpoint) {
				levelCheckpointIndex = i;
			}
			else if (biome == Biome::Dungeon) { //if tutorial level needed
				if (gameInstance->RoomBlueprints[i]->IsLevelTutorial) {
					levelTutorialIndex = i;
				}
				else if (gameInstance->RoomBlueprints[i]->GetNumberOfEntrances() == 0 &&
					gameInstance->RoomBlueprints[i]->BorderType != BorderBlockType::None) {
					if (!borderBlocks.Contains(gameInstance->RoomBlueprints[i]->BorderType)) {
						borderBlocks.Add(gameInstance->RoomBlueprints[i]->BorderType, i);
					}
				}
			}
		}
	}

	// Check if all border blocks exist
	for (int borderTypeInt = (int)BorderBlockType::TopLeft; borderTypeInt < (int)BorderBlockType::LeftAndBottom; ++borderTypeInt)
	{
		BorderBlockType currentType = (BorderBlockType)borderTypeInt;

		if (!borderBlocks.Contains(currentType))
		{
			allBorderBlocksExist = false;
			UE_LOG(LogTemp, Warning, TEXT("Missing border blocks for biome"));
			break;
		}
	}

	if (biome != Biome::Platforms) {
		if (levelEntranceIndex == -1 ||
			levelExitIndex == -1 ||
			blockIndex == -1 ||
			levelCheckpointIndex == -1) {
			UE_LOG(LogTemp, Error, TEXT("The selected biome has no entrance, exit, checkpoint or block. Please check the room blueprints and try again."));
			return;
		}
	}
	else {
		if (levelEntranceIndex == -1 ||
			levelExitIndex == -1) {
			UE_LOG(LogTemp, Error, TEXT("The selected biome has no entrance or exit. Please check the room blueprints and try again."));
			return;
		}
	}

	// Spawn a starting room
	TArray<int> possibleStartingRoomIndices;
	for (int i = 0; i < gameInstance->RoomBlueprints.Num(); ++i) {
		if (gameInstance->RoomBlueprints[i]->Biome == biome &&
			gameInstance->RoomBlueprints[i]->GetNumberOfEntrances() > 1 &&
			i != levelEntranceIndex &&
			i != levelExitIndex &&
			i != levelCheckpointIndex &&
			i != levelTutorialIndex) {
			possibleStartingRoomIndices.Add(i);
		}
	}

	if (possibleStartingRoomIndices.Num() > 0) {
		int startingRoomIndex = possibleStartingRoomIndices[randomStream->RandRange(0, possibleStartingRoomIndices.Num() - 1)];
		AddRoom(0, 0, startingRoomIndex, &openList, &collisionMap);
		placedRoomInstances.Add(startingRoomIndex, 1);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Unable to place a starting room. Check that a room exists for the biome that contains at least two entrances."));
		return;
	}

	int roomsPerCheckpoint = FMath::Abs(numberOfRooms / (numberOfCheckpoints + 1));
	int temp = roomsPerCheckpoint;
	int maxRoomNum;

	if (levelCheckpointIndex == -1) {
		maxRoomNum = numberOfRooms;
	}
	else {
		maxRoomNum = numberOfRooms + numberOfCheckpoints;
	}

	// Place the rest of the rooms
	for (int roomCount = 0; roomCount < maxRoomNum; ++roomCount) {
		// If there are still open entrances available
		if (openList.Num() > 0) {
			// Choose a random available entrance on which to join a new room
			int entranceIndex = randomStream->RandRange(0, openList.Num() - 1);

			// Populate an array of indices of rooms to consider placing
			TArray<int> roomsToConsider;

			if (roomCount == roomsPerCheckpoint + 1 && levelCheckpointIndex != -1) {
				roomsToConsider.Add(levelCheckpointIndex);
				roomsPerCheckpoint += temp;
				if (!placedRoomInstances.Contains(levelCheckpointIndex)) {
					placedRoomInstances.Add(levelCheckpointIndex, 0);
				}
			}
			else {
				for (int i = 0; i < gameInstance->RoomBlueprints.Num(); ++i) {
					// If the current room is of the required biome and has
					// more than 1 entrance then add it to the array.
					if (gameInstance->RoomBlueprints[i]->Biome == biome &&
						gameInstance->RoomBlueprints[i]->GetNumberOfEntrances() > 1 &&
						i != levelEntranceIndex &&
						i != levelExitIndex &&
						i != levelCheckpointIndex &&
						i != levelTutorialIndex) {
						roomsToConsider.Add(i);
						if (!placedRoomInstances.Contains(i)) {
							placedRoomInstances.Add(i, 0);
						}
					}
				}
			}

			bool roomPlaced = false;
			while (roomsToConsider.Num() > 0 && !roomPlaced) {
				// Pick a random room index to consider, favouring rooms that 
				// have not been placed yet.
				int currentRoomIndex = -1;
				int totalProbabilityCount = 0;
				TMap<int, float> spawnProbabilites;

				// For each room, assign a value to it representing how many
				// rooms that have been replaced aren't this one.
				for (auto iterator = placedRoomInstances.CreateConstIterator(); iterator; ++iterator)
				{
					// Only add a room possibility if this room can be spawned
					if (roomsToConsider.Contains(iterator.Key())) {
						int roomsPlaced = (*MapData).Num();
						spawnProbabilites.Add(iterator.Key(), roomsPlaced - iterator.Value());
						totalProbabilityCount += roomsPlaced - iterator.Value();
					}
				}

				// Choose a random value that will fall somewhere in the spawn
				// probabilities.
				float randomValue = randomStream->RandRange(0, totalProbabilityCount);
				bool roomChosen = false;

				// Work out which room was selected.
				for (auto iterator = spawnProbabilites.CreateConstIterator(); iterator; ++iterator)
				{
					if (iterator.Value() != 0 && !roomChosen) {
						randomValue -= iterator.Value();

						if (randomValue <= 0)
						{
							// This room has been selected
							currentRoomIndex = iterator.Key();
							roomChosen = true;
						}
					}
				}

				if (currentRoomIndex != -1) {
					// Check to see if the room can be joined at the current
					// open entrance.
					FVector2D roomPosition;
					if (CanRoomBeJoined(openList[entranceIndex], currentRoomIndex, &roomPosition, &collisionMap)) {
						// If the room can be placed then remove the current
						// entrance, which can now be considered to be closed,
						// and spawn the room.
						openList.RemoveAt(entranceIndex);
						AddRoom(roomPosition.X, roomPosition.Y, currentRoomIndex, &openList, &collisionMap);
						placedRoomInstances[currentRoomIndex] += 1;
						roomPlaced = true;
					}
					else {
						// If the room cannot be placed then remove it from the
						// array of rooms to consider.
						roomsToConsider.Remove(currentRoomIndex);
					}
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("Error when selecting random room. Room not placed."));
				}
			}

			if (!roomPlaced) {
				UE_LOG(LogTemp, Warning, TEXT("Unable to place room."));
				// TODO: Add code to block off this entrance
			}
		}
	}

	// Place the level entrance at the leftmost entrance
	// TODO: Create an array of indices in decreasing leftness in case the leftmost cannot be used
	// First we place the tutorial level
	int leftmostEntranceIndex = 0;
	FVector2D levelEntrancePosition;

	if (biome == Biome::Dungeon) {
		for (int i = 0; i < openList.Num(); ++i) {
			if (openList[i].X < openList[leftmostEntranceIndex].X) {
				leftmostEntranceIndex = i;
			}
		}
		if (CanRoomBeJoined(openList[leftmostEntranceIndex], levelTutorialIndex, &levelEntrancePosition, &collisionMap)) {
			AddRoom(levelEntrancePosition.X, levelEntrancePosition.Y, levelTutorialIndex, &openList, &collisionMap);
			openList.RemoveAt(leftmostEntranceIndex);
		}
	}


	for (int i = 0; i < openList.Num(); ++i) {
		if (openList[i].X < openList[leftmostEntranceIndex].X) {
			leftmostEntranceIndex = i;
		}
	}

	// Spawn the level entrance and remove the open entrance from the list
	if (CanRoomBeJoined(openList[leftmostEntranceIndex], levelEntranceIndex, &levelEntrancePosition, &collisionMap)) {
		AddRoom(levelEntrancePosition.X, levelEntrancePosition.Y, levelEntranceIndex, &openList, &collisionMap);
		openList.RemoveAt(leftmostEntranceIndex);

		// Get the location where the player start should be placed
		FVector2D relativePlayerPosition = gameInstance->RoomBlueprints[levelEntranceIndex]->PlayerSpawnPosition;

		// Store the player spawn position
		FVector* newSpawnPosition = ConvertBlockPositionToWorldPosition(levelEntrancePosition.X + relativePlayerPosition.X, levelEntrancePosition.Y + relativePlayerPosition.Y);
		PlayerSpawnPosition->X = newSpawnPosition->X;
		PlayerSpawnPosition->Z = newSpawnPosition->Z;

		// Ensure the player is in the centre of the room blueprints
		PlayerSpawnPosition->Y = -200;
	}

	// Place the level exit at the rightmost entrance
	// TODO: Create an array of indices in decreasing rightness in case the rightmost cannot be used
	int rightmostEntranceIndex = 0;

	for (int i = 0; i < openList.Num(); ++i) {
		if (openList[i].X > openList[rightmostEntranceIndex].X) {
			rightmostEntranceIndex = i;
		}
	}

	// Spawn the level exit and remove the open entrance from the list
	FVector2D levelExitPosition;
	if (CanRoomBeJoined(openList[rightmostEntranceIndex], levelExitIndex, &levelExitPosition, &collisionMap)) {
		AddRoom(levelExitPosition.X, levelExitPosition.Y, levelExitIndex, &openList, &collisionMap);
		openList.RemoveAt(rightmostEntranceIndex);
	}

	// Block off all remaining open entrances
	if (biome != Biome::Platforms) {
		for (int currentOpenEntranceIndex = 0; currentOpenEntranceIndex < openList.Num(); ++currentOpenEntranceIndex) {
			int entranceX = openList[currentOpenEntranceIndex].X;
			int entranceY = openList[currentOpenEntranceIndex].Y;
			if (CheckCollision(entranceX, entranceY, &collisionMap) == false) {
				AddRoom(entranceX, entranceY, blockIndex, &openList, &collisionMap);
			}
		}
	}

	// Surround each room with blocks
	if (biome != Biome::Platforms) {
		TArray<CollisionLocation>* newCollisionMap = new TArray<CollisionLocation>(collisionMap);
		TArray<FVector2D> borderBlockLocations;

		for (int i = 0; i < collisionMap.Num(); ++i)
		{
			int tileX = collisionMap[i].X;
			int tileY = collisionMap[i].Y;

			if (!CheckCollision(tileX - 1, tileY + 1, newCollisionMap)) {
				newCollisionMap->Emplace(tileX - 1, tileY + 1);
				borderBlockLocations.Add(FVector2D(tileX - 1, tileY + 1));
			}
			if (!CheckCollision(tileX, tileY + 1, newCollisionMap)) {
				newCollisionMap->Emplace(tileX, tileY + 1);
				borderBlockLocations.Add(FVector2D(tileX, tileY + 1));
			}
			if (!CheckCollision(tileX + 1, tileY + 1, newCollisionMap)) {
				newCollisionMap->Emplace(tileX + 1, tileY + 1);
				borderBlockLocations.Add(FVector2D(tileX + 1, tileY + 1));
			}
			if (!CheckCollision(tileX + 1, tileY, newCollisionMap)) {
				newCollisionMap->Emplace(tileX + 1, tileY);
				borderBlockLocations.Add(FVector2D(tileX + 1, tileY));
			}
			if (!CheckCollision(tileX + 1, tileY - 1, newCollisionMap)) {
				newCollisionMap->Emplace(tileX + 1, tileY - 1);
				borderBlockLocations.Add(FVector2D(tileX + 1, tileY - 1));
			}
			if (!CheckCollision(tileX, tileY - 1, newCollisionMap)) {
				newCollisionMap->Emplace(tileX, tileY - 1);
				borderBlockLocations.Add(FVector2D(tileX, tileY - 1));
			}
			if (!CheckCollision(tileX - 1, tileY - 1, newCollisionMap)) {
				newCollisionMap->Emplace(tileX - 1, tileY - 1);
				borderBlockLocations.Add(FVector2D(tileX - 1, tileY - 1));
			}
			if (!CheckCollision(tileX - 1, tileY, newCollisionMap)) {
				newCollisionMap->Emplace(tileX - 1, tileY);
				borderBlockLocations.Add(FVector2D(tileX - 1, tileY));
			}
		}

		for (int i = 0; i < borderBlockLocations.Num(); ++i)
		{
			int tempX = (int)borderBlockLocations[i].X;
			int tempY = (int)borderBlockLocations[i].Y;

			if (!allBorderBlocksExist) {
				AddRoom(tempX, tempY, blockIndex, &openList, newCollisionMap);
			}
			else {
				bool voidTopLeft = (!CheckCollision(tempX - 1, tempY - 1, newCollisionMap));
				bool voidTop = (!CheckCollision(tempX, tempY - 1, newCollisionMap));
				bool voidTopRight = (!CheckCollision(tempX + 1, tempY - 1, newCollisionMap));
				bool voidRight = (!CheckCollision(tempX + 1, tempY, newCollisionMap));
				bool voidBottomRight = (!CheckCollision(tempX + 1, tempY + 1, newCollisionMap));
				bool voidBottom = (!CheckCollision(tempX, tempY + 1, newCollisionMap));
				bool voidBottomLeft = (!CheckCollision(tempX - 1, tempY + 1, newCollisionMap));
				bool voidLeft = (!CheckCollision(tempX - 1, tempY, newCollisionMap));

				if (voidTop) {
					if (voidLeft) {
						AddRoom(tempX, tempY, borderBlocks[BorderBlockType::LeftAndTop], &openList, newCollisionMap);
					}
					else if (voidRight) {
						AddRoom(tempX, tempY, borderBlocks[BorderBlockType::RightAndTop], &openList, newCollisionMap);
					}
					else {
						AddRoom(tempX, tempY, borderBlocks[BorderBlockType::Top], &openList, newCollisionMap);
					}
				}
				else if (voidBottom) {
					if (voidLeft) {
						AddRoom(tempX, tempY, borderBlocks[BorderBlockType::LeftAndBottom], &openList, newCollisionMap);
					}
					else if (voidRight) {
						AddRoom(tempX, tempY, borderBlocks[BorderBlockType::RightAndBottom], &openList, newCollisionMap);
					}
					else {
						AddRoom(tempX, tempY, borderBlocks[BorderBlockType::Bottom], &openList, newCollisionMap);
					}
				}
				else if (voidLeft) {
					AddRoom(tempX, tempY, borderBlocks[BorderBlockType::Left], &openList, newCollisionMap);
				}
				else if (voidRight) {
					AddRoom(tempX, tempY, borderBlocks[BorderBlockType::Right], &openList, newCollisionMap);
				}
				else if (voidTopLeft) {
					AddRoom(tempX, tempY, borderBlocks[BorderBlockType::TopLeft], &openList, newCollisionMap);
				}
				else if (voidTopRight) {
					AddRoom(tempX, tempY, borderBlocks[BorderBlockType::TopRight], &openList, newCollisionMap);
				}
				else if (voidBottomLeft) {
					AddRoom(tempX, tempY, borderBlocks[BorderBlockType::BottomLeft], &openList, newCollisionMap);
				}
				else if (voidBottomRight) {
					AddRoom(tempX, tempY, borderBlocks[BorderBlockType::BottomRight], &openList, newCollisionMap);
				}
				else
				{
					AddRoom(tempX, tempY, blockIndex, &openList, newCollisionMap);
				}
			}
		}
	}
}
