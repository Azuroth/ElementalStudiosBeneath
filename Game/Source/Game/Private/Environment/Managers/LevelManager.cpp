// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelManager.h"
#include "MapGeneratorWorker.h"

#define BLOCK_WIDTH 400
#define BLOCK_HEIGHT 400


// Sets default values for this component's properties
ALevelManager::ALevelManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts
void ALevelManager::BeginPlay()
{
	Super::BeginPlay();

	levelReady = false;

	// Set the IsLevelLoaded flag to false
	Cast<UBeneathGameInstance>(GetGameInstance())->IsLevelLoaded = false;

	// Get a reference to the player character
	playerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	// Get a reference to the game instance
	gameInstance = Cast<UBeneathGameInstance>(GetGameInstance());

	// Assign the delegate
	callbackDelegate.BindUObject(this, &ALevelManager::LevelReadyCallback);

	// Set up the random stream
	if (gameInstance->levelSeed == -1) {
		randomStream = FRandomStream();
		randomStream.GenerateNewSeed();

		gameInstance->levelSeed = randomStream.GetCurrentSeed();
	}
	else {
		randomStream = FRandomStream(gameInstance->levelSeed);
	}

	if (!gameInstance->IsDataLoaded()) {
		gameInstance->LoadData();
	}

	GenerateLevel(NumberOfRooms, NumberOfCheckpoints, LevelBiome);
}

// Called every frame
void ALevelManager::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	if (levelReady)
	{
		CreateLevel();
		FMapGeneratorWorker::Shutdown();
		levelReady = false;
		Cast<UBeneathGameInstance>(GetGameInstance())->IsLevelLoaded = true;
	}

	//FVector playerPosition = playerCharacter->GetActorLocation();
	//int currentPlayerBlockX = playerPosition.X / BLOCK_WIDTH;
	//int currentPlayerBlockY = playerPosition.Z / BLOCK_HEIGHT;

	//// If the player has moved in block space, change which rooms are displayed
	//if (currentPlayerBlockX != playerBlockX ||
	//	currentPlayerBlockY != playerBlockY) {

	//	int worldspaceViewRange = ViewRange * BLOCK_WIDTH;

	//	TArray<int> visibleRoomIndices;

	//	for (int i = 0; i < mapData.Num(); ++i) {
	//		int currentRoomIndex = mapData[i]->RoomIndex;
	//		FVector* roomPosition = ConvertBlockPositionToWorldPosition(
	//			mapData[i]->X + roomBlueprints[currentRoomIndex]->Width / 2,
	//			mapData[i]->Y + roomBlueprints[currentRoomIndex]->Height / 2);

	//		bool isHidden = (*roomPosition - playerPosition).Size() > worldspaceViewRange;
	//		if (!isHidden) {
	//			visibleRoomIndices.Add(i);
	//		}
	//	}

	//	for (int i = 0; i < mapData.Num(); ++i) {
	//		if (mapData[i]->IsEnabled) {
	//			if (!visibleRoomIndices.Contains(i)) {
	//				mapData[i]->IsEnabled = false;
	//				mapData[i]->SpawnedRoom->Destroy();
	//			}
	//		}
	//		else {
	//			if (visibleRoomIndices.Contains(i)) {
	//				SpawnRoom(i);
	//			}
	//		}
	//	}
	//}
}

FVector * ALevelManager::ConvertBlockPositionToWorldPosition(float x, float y)
{
	// Convert a position from block space to world space. Due to the origin being
	// treated as the top left, the y co-ordinate needs to be inverted.
	return new FVector(x * BLOCK_WIDTH, 0, -y * BLOCK_HEIGHT);
}

void ALevelManager::LevelReadyCallback()
{
	UE_LOG(LogTemp, Warning, TEXT("Callback triggered."));
	levelReady = true;
}

void ALevelManager::CreateLevel()
{
	for (int i = 0; i < mapData.Num(); ++i)
	{
		SpawnRoom(i);
	}

	// Set the player position
	UE_LOG(LogTemp, Warning, TEXT("Number of rooms: %d"), mapData.Num());
	UE_LOG(LogTemp, Warning, TEXT("Player spawn position: %f, %f, %f"), PlayerSpawnPosition.X, PlayerSpawnPosition.Y, PlayerSpawnPosition.Z);

	FOutputDeviceNull arg;
	playerCharacter->CallFunctionByNameWithArguments(TEXT("Respawn"), arg, NULL, true);
	//->SetActorLocation(PlayerSpawnPosition);
}

void ALevelManager::SpawnRoom(int mapDataIndex)
{
	// Get a reference to the world
	UWorld* const world = GetWorld();

	// Get the spawn info
	int x = mapData[mapDataIndex]->X;
	int y = mapData[mapDataIndex]->Y;
	int roomIndex = mapData[mapDataIndex]->RoomIndex;

	if (world) {
		// Spawn the room
		FActorSpawnParameters spawnParams;
		FVector* position = ConvertBlockPositionToWorldPosition(x, y);
		FRotator* rotation = new FRotator(0, 0, 0);
		ARoomBlueprint* newRoom = world->SpawnActor<ARoomBlueprint>(gameInstance->RoomBlueprints[roomIndex]->GetClass(), *position, *rotation, spawnParams);

		// Add the spawned room to the array
		newRoom->BlockPositionX = x;
		newRoom->BlockPositionY = y;
		mapData[mapDataIndex]->SpawnedRoom = newRoom;
		mapData[mapDataIndex]->IsEnabled = true;

		// Spawn enemies if possible
		newRoom->SpawnEnemies(EnemySpawnChance, gameInstance->EnemyBlueprints, &randomStream);
	}
}

void ALevelManager::GenerateLevel(int numberOfRooms, int numberOfCheckpoints, Biome biome)
{
	UBeneathGameInstance* beneathGameInstance = Cast<UBeneathGameInstance>(GetGameInstance());

	FMapGeneratorWorker::InitRun(
		&mapData,
		&PlayerSpawnPosition,
		numberOfRooms,
		numberOfCheckpoints,
		biome,
		beneathGameInstance,
		callbackDelegate,
		&randomStream);
}

void ALevelManager::RespawnPlayer()
{
	UE_LOG(LogTemp, Warning, TEXT("I'm back."));

	// Set the player position
	UE_LOG(LogTemp, Warning, TEXT("Player spawn position: %f, %f, %f"), PlayerSpawnPosition.X, PlayerSpawnPosition.Y, PlayerSpawnPosition.Z);
	playerCharacter->SetActorLocation(PlayerSpawnPosition);
}

void ALevelManager::ChangeRespawnLocation(FVector location) 
{
	PlayerSpawnPosition = location;
	UE_LOG(LogTemp, Warning, TEXT("Player spawn position changed to: %f, %f, %f"), PlayerSpawnPosition.X, PlayerSpawnPosition.Y, PlayerSpawnPosition.Z);
}