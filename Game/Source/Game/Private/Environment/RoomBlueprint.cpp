// Fill out your copyright notice in the Description page of Project Settings.

#include "RoomBlueprint.h"

// Sets default values
ARoomBlueprint::ARoomBlueprint()
{
}

ARoomBlueprint::~ARoomBlueprint()
{
	// DestroyEnemies();
}

int ARoomBlueprint::GetNumberOfEntrances_Implementation()
{
	return Entrances.Num();
}

void ARoomBlueprint::SpawnEnemies(float enemyChance, TArray<ACharacter_Enemy*> possibleEnemies, FRandomStream* randomStream)
{
	TArray<UEnemySpawnPoints*> enemySpawnPointsComponents;

	this->GetComponents<UEnemySpawnPoints>(enemySpawnPointsComponents);

	if (enemySpawnPointsComponents.Num() > 0 && possibleEnemies.Num() > 0) {
		UE_LOG(LogTemp, Warning, TEXT("Spawn point component found"));

		// This room has an enemy spawn point component
		UEnemySpawnPoints* enemySpawnPoints = enemySpawnPointsComponents[0];

		TArray<FEnemyPatrolData> patrolData = enemySpawnPoints->GetPatrolData();

		for (int currentSpawnPoint = 0; currentSpawnPoint < patrolData.Num(); ++currentSpawnPoint) {
			if (randomStream->RandRange(0.0f, 1.0f) < enemyChance) {
				SpawnEnemy(
					&patrolData[currentSpawnPoint],
					possibleEnemies[randomStream->RandRange(0, possibleEnemies.Num() - 1)]);
			}
		}
	}
}

void ARoomBlueprint::DestroyEnemies()
{
	for (int i = enemies.Num() - 1; i >= 0; i--) {
		enemies[i]->Destroy();
	}
}

void ARoomBlueprint::SpawnEnemy(FEnemyPatrolData* patrolData, ACharacter_Enemy* enemyToSpawn)
{
	FActorSpawnParameters spawnParams;
	FVector position = patrolData->SpawnPoint;
	FRotator* rotation = new FRotator(0, 0, 0);
	ACharacter_Enemy* newEnemy = GetWorld()->SpawnActor<ACharacter_Enemy>(enemyToSpawn->GetClass(), position, *rotation, spawnParams);

	newEnemy->SpawnDefaultController();
	newEnemy->UpdatePatrolPoints(patrolData);

	enemies.Add(newEnemy);
}

