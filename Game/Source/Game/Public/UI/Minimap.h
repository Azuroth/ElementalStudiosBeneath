// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/SceneCapture2D.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "Minimap.generated.h"

/**
 * Developed using https://wiki.unrealengine.com/UMG_Mini-Map
 */
UCLASS()
class GAME_API AMinimap : public ASceneCapture2D
{
	GENERATED_BODY()

public:

	AMinimap();

	// Called every frame
	virtual void Tick(float deltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Minimap)
		int MinimapZoom;

protected:

	// Called when the game starts
	virtual void BeginPlay() override;

	ACharacter* playerCharacter;
};
