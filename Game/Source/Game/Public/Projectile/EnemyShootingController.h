// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Elements.h"
#include "EnemyShootingController.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAME_API UEnemyShootingController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEnemyShootingController();

	//Reference to the projectile, set within the player blueprint
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class AElementProjectile> ElementProjectile;

	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		ElementType damageType;

	void FireElementProjectile(FVector targetLocation);

private:
	float timeSinceLastShot;
	float fireRate;
	
};
