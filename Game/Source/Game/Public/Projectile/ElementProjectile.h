// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Elements.h"
#include "ElementProjectile.generated.h"

UCLASS()
class GAME_API AElementProjectile : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, Category = "ElementalProjectile")
	class USphereComponent* CollisionComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ElementalProjectile", meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovement;
	
public:	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = Projectile)
	int TimesBounced;

	AElementProjectile();

public:	
	UFUNCTION(BlueprintCallable, Category = "ElementalProjectile")
	void OnHit(UPrimitiveComponent* hitComponent, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Output")
	void SwapVisualEffect();

	void SetVel(float spd);

	//This needs to be set when you fire a projectile, so the game knows what kind of projectile is being fired
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ElementalProjectile")
	ElementType TypeOfProjectile;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AActor> FireBounce;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AActor> IceBounce;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ElementalProjectile")
	bool enemyProjectile;
};
