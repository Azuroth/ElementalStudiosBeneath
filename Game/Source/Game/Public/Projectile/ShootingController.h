// Copyright Elemental Studios 2018 ©

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Elements.h"
#include "GameFramework/Character.h"
#include "ShootingController.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAME_API UShootingController : public UActorComponent
{
	GENERATED_BODY()

public:
	// Initialisation
	UShootingController();
	void InitialiseController(AActor* actor);

	AActor* GameActor;
	APlayerController* MainActorController;
	FVector2D ScreenSize;
	FVector ShootingLocation; //Location projectile spawns from

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	bool IsFired;

	float Time;

	//Reference to the projectile, set within the player blueprint
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AElementProjectile> FireProjectile;

	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AElementProjectile> IceProjectile;

	//Get crosshair location
	UFUNCTION(BlueprintCallable)
	FVector2D GetCrosshairLocation();

	//Input controlled methods
	void XAxisMovement(float x);
	void YAxisMovement(float y);
	void FireElementProjectile(ElementType type, float speed);

private:
	//Get the angle to fire the projectile based on cursor location
	float GetFiringAngle();
};
