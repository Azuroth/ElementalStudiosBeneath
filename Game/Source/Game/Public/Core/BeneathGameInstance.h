// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "RoomBlueprint.h"
#include "Runtime/Engine/Classes/Engine/ObjectLibrary.h"
#include "Runtime/Engine/Classes/Engine/StreamableManager.h"
#include "BeneathGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API UBeneathGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UBeneathGameInstance(const FObjectInitializer& objectInitialiser);
	void LoadData();
	bool IsDataLoaded();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProceduralGeneration)
	int32 levelSeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProceduralGeneration)
	bool IsLevelLoaded;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Time)
	float Time;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Time)
	bool TimerPaused;

	// Stores the loaded room blueprints
	UPROPERTY()
	TArray<ARoomBlueprint*> RoomBlueprints;

	// Stores the loaded enemy blueprints
	UPROPERTY()
	TArray<ACharacter_Enemy*> EnemyBlueprints;

protected:
	void LoadRooms();
	void LoadEnemies();
	void LoadRoomsAsync();
	void LoadRoomsCallback();
	void LoadEnemiesAsync();
	void LoadEnemiesCallback();

	FStreamableManager streamableManager;
	UObjectLibrary* roomBlueprintLibrary;
	UObjectLibrary* enemyBlueprintLibrary;
	TArray<FAssetData> roomAssets;
	TArray<FAssetData> enemyAssets;
	bool isRoomDataLoaded;
	bool isEnemyDataLoaded;
};
