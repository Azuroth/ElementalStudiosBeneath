// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Elements.h"
#include "CharacterCPP.generated.h"

class UShootingController;

UCLASS()
class GAME_API ACharacterCPP : public ACharacter
{
	GENERATED_BODY()

		//Methods
public:
	// Sets default values for this character's properties
	ACharacterCPP();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	//Variables
	//Element Stuff
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	float ManaLevel;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	bool CanShoot = true;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	bool ShieldUp = false;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	float CurrentHealth;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	FString DisplayText;

	UPROPERTY(VisibleAnywhere, Category = Projectile, BlueprintReadWrite)
	UShootingController* ShootingControllerComponent;

	//Mana regen variables
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	bool CanRegenMana;
	float TimeKeeper;

	// Called every frame
	virtual void Tick(float deltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* playerInputComponent) override;

	//Keyboard input
	UFUNCTION(BlueprintCallable, Category = "ElementalProjectile")
	void FireIceElement();

	UFUNCTION(BlueprintCallable, Category = "ElementalProjectile")
	void FireFireElement();

	UFUNCTION(BlueprintCallable, Category = "ElementalProjectile")
	void ChargeIceProjectile();

	UFUNCTION(BlueprintCallable, Category = "ElementalProjectile")
	void ChargeFireProjectile();

	void RemoveText();

public:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	bool iceCharging;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	bool fireCharging;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	float iceCharge;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Player")
	float fireCharge;
};
