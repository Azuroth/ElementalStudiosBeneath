// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interface.h"
#include "InteractsWithElements.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractsWithElements : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
* Interface to be inherited by gameobjects which will be effected by the elements
*/
class IInteractsWithElements
{
	GENERATED_IINTERFACE_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	//Another function which doesn't have to be implemented
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Gameplay)
	void HitByIceElement();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Gameplay)
	void HitByFireElement();
};
