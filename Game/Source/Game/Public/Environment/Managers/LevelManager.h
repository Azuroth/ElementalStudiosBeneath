// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BeneathGameInstance.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Entrance.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/Blueprint.h"
#include "Runtime/Engine/Classes/Engine/BlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/SCS_Node.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "RoomBlueprint.h"
#include "RoomSpawnInformation.h"
#include "LevelManager.generated.h"

DECLARE_DELEGATE(FLevelDelegate);

UCLASS()
class GAME_API ALevelManager : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	ALevelManager();

	// Called every frame
	virtual void Tick(float deltaTime) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Converts a position from block space to world space
	FVector* ConvertBlockPositionToWorldPosition(float x, float y);

	// Callback function to signify the level is ready
	void LevelReadyCallback();

	// Function to spawn the rooms and the player
	void CreateLevel();

	// Spawns the specified room
	void SpawnRoom(int mapDataIndex);

	// Stores the map data
	TArray<RoomSpawnInformation*> mapData;

	// The current player position in blocks
	int playerBlockX;
	int playerBlockY;

	// Pointer to the player character
	ACharacter* playerCharacter;

	// Pointer to the game instance
	UBeneathGameInstance* gameInstance;

	// Callback function
	FLevelDelegate callbackDelegate;

	bool levelReady;

	FRandomStream randomStream;

public:
	// Generates the level
	void GenerateLevel(int numberOfRooms, int numberOfCheckpoints, Biome biome);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LevelManager)
		Biome LevelBiome;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LevelManager)
		int NumberOfRooms;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LevelManager)
		int NumberOfCheckpoints;

	// How far the player should be able to view rooms (in blocks)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LevelManager)
		int ViewRange;

	// Probability of enemies spawning
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LevelManager)
		float EnemySpawnChance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = LevelManager)
		FVector PlayerSpawnPosition;

	UFUNCTION(BlueprintCallable, Category = "Level Manager Functions")
		void RespawnPlayer();

	UFUNCTION(BlueprintCallable, Category = "Level Manager Functions")
		void ChangeRespawnLocation(FVector location);
};
