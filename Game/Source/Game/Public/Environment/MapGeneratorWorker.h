// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BeneathGameInstance.h"
#include "CollisionLocation.h"
#include "CoreMinimal.h"
#include "LevelManager.h"
#include "RoomSpawnInformation.h"
#include "Runtime/Core/Public/HAL/Runnable.h"

// Based on the information provided by: https://wiki.unrealengine.com/Multi-Threading:_How_to_Create_Threads_in_UE4
class FMapGeneratorWorker : FRunnable
{
	// Singleton instance
	static FMapGeneratorWorker* Runnable;

	// Thread to run on
	FRunnableThread* Thread;

	// Pointer to where to save the data
	TArray<RoomSpawnInformation*>* MapData;

	// Pointer to where to save the player spawn position
	FVector* PlayerSpawnPosition;

public:
	FMapGeneratorWorker(TArray<RoomSpawnInformation*>* dataLocation, FVector* playerSpawnPositionLocation, int newNumberOfRooms, int newNumberOfCheckpoints, Biome newBiome, UBeneathGameInstance* beneathGameInstance, FLevelDelegate newLevelDelegate, FRandomStream* randomStream);
	~FMapGeneratorWorker();

	virtual bool Init();
	virtual uint32 Run();
	virtual void Stop();

	void EnsureCompletion();

	static FMapGeneratorWorker * InitRun(TArray<RoomSpawnInformation*>* dataLocation, FVector* playerSpawnPositionLocation, int newNumberOfRooms, int newNumberOfCheckpoints, Biome newBiome, UBeneathGameInstance * beneathGameInstance, FLevelDelegate newLevelDelegate, FRandomStream* randomStream);
	static void Shutdown();
	static bool IsThreadFinished();

	bool IsFinished;

protected:
	FLevelDelegate callback;
	void GenerateLevel();
	bool CheckCollision(int x, int y, TArray<CollisionLocation>* collisionMap);
	void AddRoom(int x, int y, int roomIndex, TArray<FEntrance>* openList, TArray<CollisionLocation>* collisionMap);
	void GetEntrancesForRoom(int roomIndex, TArray<FEntrance>* newEntrances, bool getExternal = false);
	bool CanRoomBeJoined(FEntrance entranceToJoinOn, int roomIndex, FVector2D * roomPosition, TArray<CollisionLocation>* collisionMap);

	FVector * ConvertBlockPositionToWorldPosition(float x, float y);

	int numberOfRooms;
	int numberOfCheckpoints;
	Biome biome;
	UBeneathGameInstance* gameInstance;
	FRandomStream* randomStream;
};
