// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyPatrolData.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct GAME_API FEnemyPatrolData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Metadata")
		FVector SpawnPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Metadata")
		TArray<FVector> PatrolPoints;

	// Getters
	FEnemyPatrolData() 
	{
		SpawnPoint = FVector(0, 0, 0);
	}
};
