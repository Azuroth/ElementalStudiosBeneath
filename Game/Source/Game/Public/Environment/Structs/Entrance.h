// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EntranceType.h"
#include "Vector2D.h"
#include "Entrance.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct GAME_API FEntrance
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Metadata")
	int X;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Metadata")
	int Y;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Metadata")
	EntranceType Type;

	// Getters

	FVector2D* GetPosition()
	{
		int tempX = X;
		int tempY = Y;

		switch (Type) {

		case EntranceType::Top:
			--tempY;
			break;

		case EntranceType::Right:
			++tempX;
			break;

		case EntranceType::Bottom:
			++tempY;
			break;

		case EntranceType::Left:
			--tempX;
			break;
		}

		return new FVector2D(tempX, tempY);
	}

	// Default constructor
	FEntrance()
	{
		X = 0;
		Y = 0;
		Type = EntranceType::Bottom;
	}

	// Constructor
	FEntrance(int x, int y, EntranceType type)
	{
		X = x;
		Y = y;
		Type = type;
	}
};
