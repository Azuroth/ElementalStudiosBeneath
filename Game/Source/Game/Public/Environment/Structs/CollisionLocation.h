// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

// Used because UE4's TArrays can't handle multi-dimensional arrays
// See: https://wiki.unrealengine.com/Dynamic_Arrays#Multi_Dimensional_Arrays
struct CollisionLocation
{
	int X;
	int Y;

	CollisionLocation(int x, int y)
	{
		X = x;
		Y = y;
	}

	FORCEINLINE bool operator==(const CollisionLocation &other) const {
		return other.X == X && other.Y == Y;
	}
};
