// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RoomBlueprint.h"

struct RoomSpawnInformation
{
	int X;
	int Y;
	bool IsEnabled;
	int RoomIndex;
	ARoomBlueprint* SpawnedRoom;

	RoomSpawnInformation(int x, int y, int roomIndex, bool isEnabled = false)
	{
		X = x;
		Y = y;
		IsEnabled = isEnabled;
		RoomIndex = roomIndex;
	}
};
