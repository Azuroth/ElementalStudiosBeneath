// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Biome.h"
#include "BorderBlockType.h"
#include "Character_Enemy.h"
#include "CoreMinimal.h"
#include "EnemySpawnPoints.h"
#include "Entrance.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/AIModule/Classes/Blueprint/AIBlueprintHelperLibrary.h"
#include "RoomBlueprint.generated.h"

UCLASS()
class GAME_API ARoomBlueprint : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Properties")
	int Width;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Properties")
	int Height;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Properties")
	TArray<FEntrance> Entrances;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Properties")
	Biome Biome;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Properties")
	bool IsLevelEntrance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Properties")
	bool IsLevelExit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Properties")
	bool IsLevelCheckpoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Properties")
	bool IsLevelTutorial;

	// Only needed if this room is an entrance
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Properties")
	FVector2D PlayerSpawnPosition;

	// Only needed if this room is a border block
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Properties")
	BorderBlockType BorderType;

	UFUNCTION(BlueprintNativeEvent, Category = "Room Functions")
	int GetNumberOfEntrances();
	int GetNumberOfEntrances_Implementation();

	void SpawnEnemies(float enemyChance, TArray<ACharacter_Enemy*> possibleEnemies, FRandomStream* randomStream);

	void DestroyEnemies();

	// Sets default values for this actor's properties
	ARoomBlueprint();

	// Destructor
	~ARoomBlueprint();

	int BlockPositionX;
	int BlockPositionY;

protected:
	TArray<ACharacter_Enemy*> enemies;

	void SpawnEnemy(FEnemyPatrolData* patrolData, ACharacter_Enemy* enemyToSpawn);
};
