// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "EntranceType.generated.h"

// Represents possible entrance locations for a chunk
UENUM(BlueprintType)
enum class EntranceType : uint8
{
	Top UMETA(DisplayName="Top"),
	Left UMETA(DisplayName = "Left"),
	Right UMETA(DisplayName = "Right"),
	Bottom UMETA(DisplayName = "Bottom")
};
