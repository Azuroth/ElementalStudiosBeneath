// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "AIStates.generated.h"

// Stores the possible biome types for the environment
UENUM(BlueprintType)
enum class AIStates : uint8
{
	Patrol UMETA(DisplayName = "Patrol"),
	Attack_Patrol UMETA(DisplayName = "Attack & Patrol"),
	Chase_Patrol UMETA(DisplayName = "Chase & Patrol")
};
