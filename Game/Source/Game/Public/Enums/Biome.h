// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "Biome.generated.h"

// Stores the possible biome types for the environment
UENUM(BlueprintType)
enum class Biome : uint8
{
	Cave UMETA(DisplayName = "Cave"),
	Dungeon UMETA(DisplayName = "Dungeon"),
	Platforms UMETA(DisplayName = "Platforms")
};
