// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "BorderBlockType.generated.h"

// Represents the different border types for the border blocks. The values
// indicate which side has a black border.
UENUM(BlueprintType)
enum class BorderBlockType : uint8
{
	None UMETA(DisplayName = "None"),
	TopLeft UMETA(DisplayName = "Top Left"),
	Top UMETA(DisplayName = "Top"),
	TopRight UMETA(DisplayName = "Top Right"),
	Right UMETA(DisplayName = "Right"),
	BottomRight UMETA(DisplayName = "Bottom Right"),
	Bottom UMETA(DisplayName = "Bottom"),
	BottomLeft UMETA(DisplayName = "Bottom Left"),
	Left UMETA(DisplayName = "Left"),
	LeftAndTop UMETA(DisplayName = "Left and Top"),
	RightAndTop UMETA(DisplayName = "Right and Top"),
	RightAndBottom UMETA(DisplayName = "Right and Bottom"),
	LeftAndBottom UMETA(DisplayName = "Left and Bottom")
};
