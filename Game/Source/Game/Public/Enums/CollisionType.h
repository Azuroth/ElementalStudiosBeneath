// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "CollisionType.generated.h"

// Represents possible collision map values
UENUM(BlueprintType)
enum class CollisionType : uint8
{
	NotPresent UMETA(DisplayName="Not Present"),
	Passable UMETA(DisplayName="Passable"),
	Impassable UMETA(DisplayName="Impassable")
};
