// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractsWithElements.h"
#include "BurnableCrate.generated.h"

UCLASS()
class GAME_API ABurnableCrate : public AActor, public IInteractsWithElements
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABurnableCrate();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float deltaTime) override;

	virtual void HitByIceElement_Implementation() override;

	virtual void HitByFireElement_Implementation() override;

private:

	bool OnFire;

	float CurrentTime;

	float SecondsTillDeath;
};
