// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyPatrolData.h"
#include "Components/SceneComponent.h"
#include "EnemySpawnPoints.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAME_API UEnemySpawnPoints : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEnemySpawnPoints();

	TArray<FEnemyPatrolData> GetPatrolData();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	

private:

	void CreatePatrolDataList();
	TArray<FEnemyPatrolData> patrolData;
	
};
