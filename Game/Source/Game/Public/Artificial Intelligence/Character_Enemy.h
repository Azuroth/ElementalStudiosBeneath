// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyPatrolData.h"
#include "GameFramework/Character.h"
#include "Classes/Components/PointLightComponent.h"
#include "EnemyShootingController.h"
#include "Kismet/GameplayStatics.h"
#include "AIStates.h"
#include "Character_Enemy.generated.h"

UCLASS()
class GAME_API ACharacter_Enemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacter_Enemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void BeginOverlap(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult &SweepResult);
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Enemy")
		 void RotationFinished();

	UFUNCTION(BlueprintImplementableEvent, Category = "Enemy")
		void ChangeEnemyState(AIStates newState);

	UFUNCTION(BlueprintImplementableEvent, Category = "Enemy")
		void FireShot();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		FVector SpawnLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float shotTimer;

	UPROPERTY(VisibleAnywhere, Category = Projectile, BlueprintReadWrite)
		UEnemyShootingController* EnemyShootingComponent;
	
	void UpdatePatrolPoints(FEnemyPatrolData* patrolData);

	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		bool burstShotSequence;

	UPointLightComponent* chargePointLightRef;

private:
	UFUNCTION(BlueprintCallable)
		void SetTargetRotation(FVector startPos, FVector targetPos);

	UFUNCTION(BlueprintCallable)
		void StartRotation();

	UFUNCTION(BlueprintCallable)
		void Fire(FVector targetLocation);

	UFUNCTION(BlueprintCallable)
		bool CanBeginPathFind();

	UFUNCTION(BlueprintCallable)
		bool GenerateNavMesh();

	UFUNCTION(BlueprintCallable)
		FVector GetNextPatrolPoint();

	UFUNCTION(BlueprintCallable)
		void LightPointRef(UPointLightComponent* pointRef);

	UFUNCTION(BlueprintCallable)
		void ChargeShot(bool modifier);

	UFUNCTION(BlueprintCallable)
		void PassPlayerReference(ACharacter* playerRef);

	void InterpToTargetRotation(float deltaTime);
	void ChargeShot(float deltaTime);
	void ChangeEnemySize(float deltaTime);	

	AController* controllerAI;
	ACharacter* playerCharacterRef;
	TArray<FVector> patrolPoints;
	int targetPatrolPoint = 0;

	FRotator targetRot;
	bool rotateToPoint;	
	bool startChargingShot;
	bool startEnemySizeChange;
	bool enlarged;
	float chargeTimer;
	float additionToAdd;
	float scaleLevel;
};
