// Copyright Elemental Studios 2018 ©

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"

/**
 * 
 */
class GAME_API Timer : public UClass
{
public:
	Timer();
	~Timer();

private: 
	float CurrentMilliseconds;
	bool Paused;

public: 
	UFUNCTION(BlueprintCallable, Category = "Timer")
	int GetSeconds();

	UFUNCTION(BlueprintCallable, Category = "Timer")
	int GetMinutes();

	UFUNCTION(BlueprintCallable, Category = "Timer")
	void TimerTick(float deltaTime);

	UFUNCTION(BlueprintCallable, Category = "Timer")
	void ResetTimer();

	UFUNCTION(BlueprintCallable, Category = "Timer")
	void SetTimerState(bool paused);
};
