// Fill out your copyright notice in the Description page of Project Settings.

#include "GameGameModeBase.h"
#include "Timer.h"


AGameGameModeBase::AGameGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;

	//Create instance of the timer class, starts off paused
	GameTimer = Cast<Timer>(Timer::StaticClass());
}

AGameGameModeBase::~AGameGameModeBase()
{
	
}

//Tick event for the game. Sets the timer if the timer isn't paused. 
void AGameGameModeBase::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	GameTimer->TimerTick(deltaTime);
}


