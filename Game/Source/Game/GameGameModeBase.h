// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameGameModeBase.generated.h"

class Timer;

UCLASS()
class GAME_API AGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGameGameModeBase();
	~AGameGameModeBase();

public:
	virtual void Tick(float deltaTime) override;

	Timer* GameTimer;
};
